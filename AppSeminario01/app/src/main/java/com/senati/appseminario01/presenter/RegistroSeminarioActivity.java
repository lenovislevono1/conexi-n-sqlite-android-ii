package com.senati.appseminario01.presenter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.senati.appseminario01.R;

public class RegistroSeminarioActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_seminario);
    }
}
